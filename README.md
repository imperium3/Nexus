# Imperium Nexus

Welcome to the home of Imperium's **Nexus**. Nexus is designed to be a one stop place for all sorts of data. Utilizing Elasticsearch the goal of this project is to have a single place that users can import data. Utilizing custom agents the Nexus will be a place users can import anything they possibly need.

This is the public repository of this project which will get updates from Imperiums Private repositories.



